

public class App {
    public static void main(String[] args) throws Exception {
        int a=(Integer.parseInt(args[0])) ;
        esPrimo(a);
        
        System.out.println("El numero anterior "+(a-1)+ " es primo: " +esPrimo(a-1));
        System.out.println("El numero "+a+ " es primo: " +esPrimo(a));
        System.out.println("El numero posterior "+(a+1)+ " es primo: " +esPrimo(a+1));

        
        
    }
    /** Devuelve true si el numero es primo o false si no lo es 
     * @param a
     * @return 
    */
        public static boolean esPrimo (int a){
            boolean verdadero=true;
            int divisor=2;
            if (a>3){

           
            do{
                if (a % divisor==0){
                    verdadero=false;
                }
                divisor++;
            }while(divisor<=(a/2)+1 && verdadero );
        }
            return verdadero;
        
        }
    
    }

